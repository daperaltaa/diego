<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/articuloDAO.php";

class articulo{
    private $id;
    private $titulo;
    private $descripci�n;
    private $fecha;
    private $conexion;
    private $articuloDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getDescripci�n()
    {
        return $this->descripci�n;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function articulo($id="",$titulo="",$descripci�n="",$fecha=""){
        $this->id=$id;
        $this->titulo=$titulo;
        $this->descripci�n=$descripci�n;
        $this->fecha=$fecha;
        $this -> conexion = new Conexion();
        $this -> articuloDAO = new articuloDAO ($this->id,$this->titulo,$this->descripci�n,$this->fecha);
        
    }
    public function Insertar(){
        $this -> conexion -> abrir();
        echo $this -> articuloDAO -> Insertar();
        $this -> conexion -> ejecutar($this -> articuloDAO -> Insertar());
        $this -> conexion -> cerrar(); 
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarPaginacion($cantidad, $pagina));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new articulo($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> articuloDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}
?>