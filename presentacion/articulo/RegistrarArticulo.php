<?php
$Titulo = "";
if (isset($_POST["Titulo"])) {
    $Titulo = $_POST["Titulo"];
    echo $Titulo;
}

$Descripcion = "";
if (isset($_POST["Descripcion"])) {
    $Descripcion = $_POST["Descripcion"];
    echo $Descripcion;
}

$Fecha = "";
if (isset($_POST["Fecha"])) {
    $Fecha = $_POST["Fecha"];
    echo $Fecha;
}

if (isset($_POST["ingresar"])) {
    $articulo = new articulo("",$Titulo,$Descripcion,$Fecha);
    $articulo->Insertar();
}

?>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-3 col-md-0 col-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header">Registrar</div>
				<div class="card-body text-dark bg-light">
					<form
						action="index.php?pid=<?php echo base64_encode("presentacion/articulo/RegistrarArticulo.php") ?>"
						method="post">

						<div class="form-group">
							<label for="">Titulo</label> <input name="Titulo" type="text"
								class="form-control" placeholder="Titulo" required>

						</div>

						<div class="form-group">
							<label for="">Descripcion</label> <input name="Descripcion"
								type="text" class="form-control" placeholder="Descripcion"
								required>

						</div>


						<input type="date" id="start" name="Fecha" value="2018-07-22"
							min="2018-01-01" max="2018-12-31">


						<div class="form-group">
							<input name="ingresar" type="submit"
								class="form-control btn btn-info">
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>