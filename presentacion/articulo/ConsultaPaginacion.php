<?php
require_once "logica/articulo.php";
$articulo = new articulo();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$articulos = $articulo -> consultarPaginacion($cantidad, $pagina);
$totalRegistros = $articulo -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Producto</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($articulos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>edad</th>
							<th>Correo</th>
						</tr>
						<?php 
						$i=1;
						foreach($articulos as $productoActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getId() . "</td>";
						    echo "<td>" . $productoActual -> getTitulo() . "</td>";
						    echo "<td>" . $productoActual -> getDescripci�n() . "</td>";
						    echo "<td>" . $productoActual -> getFecha() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination justify-content-center"><!-- justify-content-center deja en el centro la barra -->
        						<li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/Cliente/ConsultarPaginacionSencilla.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> Anterior </a></li><!--Siguiente puede ser cambaido por: &lt;&lt;(<<);   -->
        						<?php 
        						for($i=1; $i<=$totalPaginas; $i++){
        						    if($i==$pagina){
        						        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
        						    }else{
        						        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Cliente/ConsultarPaginacionSencilla.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
        						    }        						            						    
        						}        						
        						?>
        						<li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/Cliente/ConsultarPaginacionSencilla.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> Siguiente </a></li><!--Siguiente puede ser cambaido por: &gt;&gt(>>);   -->
        					</ul>
        				</nav>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>